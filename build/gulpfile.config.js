var SRC_DIR = './src/';     // 源文件目录
var DIST_DIR = './dist/';   // 文件处理后存放的目录
var TMP_DIR = './TMP/';   // 文件处理后存放的目录
var DIST_FILES = DIST_DIR + '**'; // 目标路径下的所有文件

var REV_DIR = DIST_DIR + 'rve/'; // 压缩前图片文件夹

var Config = {
    src: SRC_DIR,
    dist: DIST_DIR,
    tmp: TMP_DIR,
    dist_files: DIST_FILES,
    rev: REV_DIR,
    rev_json: REV_DIR + '**/*.json',
    html: {
        dir: SRC_DIR,
        src: SRC_DIR + '*.html',
        dist: DIST_DIR,
        tmp: TMP_DIR
    },
    assets: {
        dir: SRC_DIR + 'assets',
        src: SRC_DIR + 'assets/**/*',            // assets目录：./src/assets
        dist: DIST_DIR + 'assets',                // assets文件build后存放的目录：./dist/assets
        tmp: TMP_DIR + 'assets'
    },
    sass: {
        dir: SRC_DIR + 'sass',
        src: SRC_DIR + 'sass/**/*.scss',         // SASS目录：./src/sass/
        dist: DIST_DIR + 'css',                   // SASS文件生成CSS后存放的目录：./dist/css
        tmp: TMP_DIR + 'css'
    },
    js: {
        dir: SRC_DIR + 'js',
        src: SRC_DIR + 'js/**/*.js',             // JS目录：./src/js/
        dist: DIST_DIR + 'js',                   // JS文件build后存放的目录：./dist/js
        tmp: TMP_DIR + 'js',
        build_name: 'build.js'                   // 合并后的js的文件名

    },
    img: {
        dir: SRC_DIR + 'images',
        src: SRC_DIR + 'images/**/*',            // images目录：./src/images/
        dist: DIST_DIR + 'images',                // images文件build后存放的目录：./dist/images
        tmp: TMP_DIR + 'images'
    },
    fonts: {
        dir: SRC_DIR + 'fonts',
        src: SRC_DIR + 'fonts/**/*',            // images目录：./src/images/
        dist: DIST_DIR + 'fonts',                // images文件build后存放的目录：./dist/images
        tmp: TMP_DIR + 'fonts'
    }
};

module.exports = Config;
