let gulp = require('gulp');
let autoprefixer = require('gulp-autoprefixer'); // 处理css中浏览器兼容的前缀
let cssmin = require('gulp-clean-css');//压缩CSS为一行
let sass = require('gulp-sass'); //sass
let uglify = require('gulp-uglify'); //js压缩
let htmlMin = require('gulp-htmlmin');//压缩html
let babel = require("gulp-babel");//es6->es5
let csscomb = require('gulp-csscomb'); //css排序
let del = require('del');//删除
let plumber = require('gulp-plumber');//阻止gulp停止
let notify = require("gulp-notify");//log
let assetRev = require('gulp-asset-rev');
let smushit = require('gulp-smushit');//图片压缩
let Config = require('./gulpfile.config.js');
let cache = require('gulp-cache');
let runSequence = require('run-sequence');
let rev = require('gulp-rev');
let revCollector = require('gulp-rev-collector');

let fontSpider = require('gulp-font-spider');

//======= gulp build 打包资源 ===============
function prod() {
    /**
     * assets文件夹下的所有文件处理
     */
    gulp.task('assets', function () {
        return gulp.src(Config.assets.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(gulp.dest(Config.assets.dist));
    });

    /**
     * assets文件夹下的所有文件处理
     */
    gulp.task('fonts', () => {
        return gulp.src(Config.fonts.src)
            .pipe(gulp.dest(Config.fonts.dist));
    });
    /**
     * SASS样式处理
     */
    gulp.task('sass', function () {
        return gulp.src(Config.sass.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(revCollector())
            .pipe(sass())
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(csscomb())
            .pipe(cssmin({
                advanced: false,//类型：Boolean 默认：true [是否开启高级优化（合并选择器等）]
                compatibility: 'ie7',//保留ie7及以下兼容写法 类型：String 默认：''or'*' [启用兼容模式； 'ie7'：IE7兼容模式，'ie8'：IE8兼容模式，'*'：IE9+兼容模式]
                keepBreaks: true,//类型：Boolean 默认：false [是否保留换行]
                keepSpecialComments: '*'
                //保留所有特殊前缀 当你用autoprefixer生成的浏览器前缀，如果不加这个参数，有可能将会删除你的部分前缀
            }))
            .pipe(rev())
            .pipe(gulp.dest(Config.sass.dist))
            .pipe(rev.manifest({
                path: 'rev-manifest-css.json'
            }))
            .pipe(gulp.dest(Config.rev));
    });
    /**
     * js处理
     */
    gulp.task('js', function () {
        return gulp.src(Config.js.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(revCollector())
            .pipe(babel())
            .pipe(uglify())
            .pipe(rev())
            .pipe(gulp.dest(Config.js.dist))
            .pipe(rev.manifest({
                path: 'rev-manifest-js.json'
            }))
            .pipe(gulp.dest(Config.rev));
    });
    /**
     * 图片处理
     */
    gulp.task('images', function () {
        return gulp.src(Config.img.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(gulp.dest(Config.img.dist));
    });

    /**
     * HTML处理
     */
    gulp.task('html', function () {

        let options = {
            removeComments: true,//清除HTML注释
            collapseWhitespace: true,//压缩HTML
            removeScriptTypeAttributes: true,//删除<script>的type="text/javascript"
            removeStyleLinkTypeAttributes: true,//删除<style>和<link>的type="text/css"
            minifyJS: true,//压缩页面JS
            minifyCSS: true//压缩页面CSS
        };
        return gulp.src([Config.rev_json, Config.html.src])
            .pipe(revCollector())
            .pipe(htmlMin(options))
            .pipe(gulp.dest(Config.html.dist));
    });

    gulp.task('fontspider', function () {
        return gulp.src(Config.html.dist)  //只要告诉它html文件所在的文件夹就可以了，超方便
            .pipe(fontSpider());
    });
    gulp.task('build', runSequence(
        ['assets'],
        ['images'],
        ['fonts'],
        ['js'],
        ['sass'],
        ['html'],
        ['fontspider']));
}

module.exports = prod;

