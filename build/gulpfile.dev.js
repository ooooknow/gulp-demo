let gulp = require('gulp');
let autoprefixer = require('gulp-autoprefixer'); // 处理css中浏览器兼容的前缀
let changed = require('gulp-changed');//检查改变状态
let sass = require('gulp-sass'); //sass
let browserSync = require('browser-sync').create();
let reload = browserSync.reload;
let Config = require('./gulpfile.config.js');

let babel = require("gulp-babel");//es6->es5

let smushit = require('gulp-smushit');//图片压缩
let csscomb = require('gulp-csscomb'); //css排序
let del = require('del');//删除

let plumber = require('gulp-plumber');//阻止gulp停止
let notify = require("gulp-notify");//log

//======= gulp dev 开发环境下 ===============
function dev() {

    gulp.task('delete', function (cb) {
        return del(['dist/*'], cb);
    });
    /**
     * HTML处理
     */
    gulp.task('html:dev', function () {
        return gulp.src(Config.html.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(gulp.dest(Config.html.tmp))
            .pipe(reload({
                stream: true
            }));
    });
    /**
     * assets文件夹下的所有文件处理
     */
    gulp.task('assets:dev', function () {
        return gulp.src(Config.assets.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(gulp.dest(Config.assets.tmp))
            .pipe(reload({
                stream: true
            }));
    });
    /**
     * assets文件夹下的所有文件处理
     */
    gulp.task('fonts:dev', () => {
        return gulp.src(Config.fonts.src)
            .pipe(gulp.dest(Config.fonts.tmp));
    });
    /**
     * SASS样式处理
     */
    gulp.task('sass:dev', function () {
        return gulp.src(Config.sass.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(changed('dist', {hasChanged: changed.compareSha1Digest}))
            .pipe(sass())
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(csscomb())
            .pipe(gulp.dest(Config.sass.tmp))
            .pipe(reload({
                stream: true
            }));
    });
    /**
     * js处理
     */
    gulp.task('js:dev', function () {
        return gulp.src(Config.js.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(changed('dist', {hasChanged: changed.compareSha1Digest}))
            .pipe(babel())
            .pipe(gulp.dest(Config.js.tmp))
            .pipe(reload({
                stream: true
            }));
    });
    /**
     * 图片处理
     */
    gulp.task('images:dev', function () {
        return gulp.src(Config.img.src)
            .pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')})) // 错误提示
            .pipe(changed('dist', {hasChanged: changed.compareSha1Digest}))
            .pipe(gulp.dest(Config.img.tmp))
            .pipe(reload({
                stream: true
            }));
    });
    gulp.task('dev', ['html:dev', 'sass:dev', 'fonts:dev', 'js:dev', 'assets:dev', 'images:dev'], function () {
        browserSync.init({
            port: 3001,
            server: {
                baseDir: Config.tmp
            }
            , notify: true
        });
        // Watch .html files
        gulp.watch(Config.html.src, ['html:dev']);
        // Watch .scss files
        gulp.watch(Config.sass.src, ['sass:dev']);
        gulp.watch(Config.sass.src, ['fonts:dev']);
        // Watch assets files
        gulp.watch(Config.assets.src, ['assets:dev']);
        // Watch .js files
        gulp.watch(Config.js.src, ['js:dev']);
        // Watch image files
        gulp.watch(Config.img.src, ['images:dev']);
    });
}

//======= gulp dev 开发环境下 ===============
module.exports = dev;
